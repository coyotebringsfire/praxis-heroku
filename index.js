var express = require('express');
var app = express();
var fs = require('fs');

import { getGame, getGames, getRole, getRoles } from './lib/praxis.js';

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
// app.set('views', __dirname + '/views');
// app.set('view engine', 'ejs');

app.get('/games', getGames);

app.get('/games/:game', getGame);

app.get('/games/:game/roles', getRoles);

app.get('/games/:game/roles/:role', getRole);

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
