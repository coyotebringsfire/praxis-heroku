"use strict";
const should = require('should');
const rimraf = require('rimraf');
const fs = require('fs');

import { getGames, getGame, getRoles, getRole, uploadPlayer } from '../lib/praxis.js';

describe("getGames", () => {
  before((done) => {
    rimraf('./public/data/test/', done);
  });
  it( "should return a list of games", (done) => {
    const mockRequest = {};
    const mockResponse = {
      status: function() {
        return {
          send: function() {
            should.fail("unexpected call to response.status.send");
          }
        };
      },
      json: function(responseObject) {
        // verify the contents of the responseObject
        fs.readdir(`./public/data/`, (err, files) => {
          responseObject.length.should.equal(files.length);
          done();
        });
      }
    };
    getGames(mockRequest, mockResponse);
  });
  it( "should include the name for each game", (done) => {
    const mockRequest = {};
    const mockResponse = {
      status: function() {
        return {
          send: function() {
            should.fail("unexpected call to response.status.send");
          }
        };
      },
      json: function(responseObject) {
        // verify the contents of the responseObject
        fs.readdir(`./public/data/`, (err, files) => {
          responseObject.length.should.equal(files.length);
          for( const k of Object.keys(responseObject) ) {
            should(responseObject[k]).be.ok;
            responseObject[k].should.equal(files[k]);
          }
          done();
        });
      }
    };
    getGames(mockRequest, mockResponse);
  });
  it( "should return an error if there is a problem getting the Games", (done) => {
    const mockRequest = {};
    const mockResponse = {
      status: function(statusCode) {
        statusCode.should.equal(500);
        return {
          send: function(message) {
            message.should.match(/^EACCES: permission denied.*/);
            fs.chmodSync('./public/data', '755');
            done();
          }
        };
      },
      json: function(responseObject) {
        should.fail("unexpected call to response.json");
      }
    };
    // create a test file structure
    fs.chmodSync('./public/data', '000');
    getGames(mockRequest, mockResponse);
  });
});

describe("getGame", () => {
  beforeEach((done) => {
    rimraf('./public/data/test/', done);
  });
  it("should return the requested game", (done) => {
    const mockRequest = {
      params: {
        game: "test"
      }
    };
    const mockResponse = {
      status: function() {
        return {
          send: function(message) {
            should.fail(`unexpected call to response.status.send ${message}`);
          }
        }
      },
      json: function(responseData) {
        should(responseData.Roles).be.ok;
        responseData.Roles[0].should.equal("test");
        done();
      }
    };
    fs.mkdirSync('./public/data/test/');
    let testFileDescriptor = fs.openSync('./public/data/test/test', 'w');
    fs.writeFileSync(testFileDescriptor, Buffer.from( JSON.stringify({"test":"test"}) ) );
    fs.closeSync(testFileDescriptor);
    getGame(mockRequest, mockResponse);
  });
  it("should return an error if requested game does not exist", (done) => {
    const mockRequest = {
      params: {
        game: "test"
      }
    };
    const mockResponse = {
      status: function(statusCode) {
        statusCode.should.equal(404);
        return {
          send: function(message) {
            message.should.match(/ENOENT: no such file or directory.*/);
            done();
          }
        }
      },
      json: function(responseData) {
        should.fail(`unexpected call to response.json ${JSON.stringify(responseData)}`);
      }
    };
    rimraf('./public/data/test/', () => {
      getGame(mockRequest, mockResponse);
    });
  });
});

describe("getRoles", () => {
  before((done) => {
    rimraf('./public/data/test/', () => {
      // create a test role
      fs.mkdirSync('./public/data/test/');
      let testFileDescriptor = fs.openSync('./public/data/test/test', 'w');
      fs.writeFileSync(testFileDescriptor, Buffer.from( JSON.stringify({"name":"test"}) ) );
      fs.closeSync(testFileDescriptor);
      done();
    });
  });
  it( "should return a list of roles for the specified game", (done) => {
    const mockRequest = {
      params: {
        game: "test"
      }
    };
    const mockResponse = {
      status: function() {
        return {
          send: function() {
            should.fail("unexpected call to response.status.send");
          }
        };
      },
      json: function(responseObject) {
        // verify the contents of the responseObject
        fs.readdir(`./public/data/test/`, (err, files) => {
          files.length.should.equal(responseObject.length);
          files[0].should.equal(responseObject[0]);
          done();
        });
      }
    };
    getRoles(mockRequest, mockResponse);
  });
  it( "should return an error if there is a problem getting the Roles requested", (done) => {
    const mockRequest = {
      params: {
        game: 'test'
      }
    };
    const mockResponse = {
      status: function(statusCode) {
        statusCode.should.equal(500);
        return {
          send: function(message) {
            message.should.match(/^EACCES: permission denied.*/);
            fs.chmodSync('./public/data/test/', '755');
            rimraf('./public/data/test/', done);
          }
        };
      },
      json: function(responseObject) {
        fs.chmodSync('./public/data/test/', '755');
        rimraf('./public/data/test/', () => {
          should.fail("unexpected call to response.json");
          done();
        });
      }
    };
    // create a test file structure
    fs.chmodSync('./public/data/test/', '000');
    getRoles(mockRequest, mockResponse);
  });
});

describe("getRole", () => {
  beforeEach((done) => {
    rimraf('./public/data/test/', done);
  });
  it("should return the requested role", (done) => {
    const mockRequest = {
      params: {
        game: "test",
        role: "test"
      }
    };
    const mockResponse = {
      status: function() {
        return {
          send: function(message) {
            should.fail(`unexpected call to response.status.send ${message}`);
          }
        }
      },
      json: function(responseData) {
        should(responseData.name).be.ok;
        responseData.name.should.equal("test");
        done();
      }
    };
    fs.mkdirSync('./public/data/test/');
    let testFileDescriptor = fs.openSync(__dirname+'/../public/data/test/test', 'w');
    fs.writeFileSync(testFileDescriptor, Buffer.from( JSON.stringify({"name":"test"}) ) );
    fs.closeSync(testFileDescriptor);
    getRole(mockRequest, mockResponse);
  });
  it("should return an error if requested role does not exist", (done) => {
    const mockRequest = {
      params: {
        game: "test",
        role: "test"
      }
    };
    const mockResponse = {
      status: function(statusCode) {
        statusCode.should.equal(404);
        return {
          send: function(message) {
            message.should.match(/ENOENT: no such file or directory/);
            done();
          }
        }
      },
      json: function(responseData) {
        should.fail(`unexpected call to response.json ${JSON.stringify(responseData)}`);
      }
    };
    rimraf('./public/data/test/', () => {
      getRole(mockRequest, mockResponse);
    });
  });
});
