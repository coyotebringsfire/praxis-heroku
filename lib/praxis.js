"use strict";
const fs = require('fs');
const bunyan = require('bunyan');
const log = bunyan.createLogger({name: "praxis"});

export function getGame(request, response) {
  fs.readdir(`./public/data/${request.params.game}`, (err, files) => {
    if( err ) {
      log.error({err}, "caught exception");
      return response.status(404)
        .send(`${err.message}`);
    }
    response.json({ Roles: files });
  });
}

export function getGames(request, response) {
  try {
    fs.readdir(`./public/data/`, (err, files) => {
      if( err ) {
        log.error({err}, "caught exception");
        return response.status(500)
          .send(`${err.message}`);
      }
      response.json(files);
    });
  } catch(err) {
    log.error({err}, "caught exception");
    return response.status(500)        // HTTP status 500: internal error
      .send(`${err.message}`);
  }
}

export function getRole(request, response) {
  fs.readFile(`./public/data/${request.params.game}/${request.params.role}`, (err, role) => {
    if( err ) {
      log.error({err}, "caught exception");
      return response.status(404)
        .send(`${err.message}`);
    }
    response.json(JSON.parse(role));
  });
}

export function getRoles(request, response) {
  log.info(`getting roles for ${request.params.game}`)
  try {
    fs.readdir(`./public/data/${request.params.game}/`, (err, files) => {
      log.info(`files read ${JSON.stringify(files)}`)
      if( err ) {
        log.error({err}, "caught exception");
        return response.status(500)
          .send(`${err.message}`);
      }
      response.json(files);
    });
  } catch(err) {
    log.error({err}, "caught exception");
    return response.status(404)        // HTTP status 404: internal error
      .send(`${err.message}`);
  }
}
