var PraxisGlobal = {
  availableGames: [],
  landingPage: document.querySelector("div[id=landingpage]"),
  game: document.querySelector("div[id=game]"),
  gamePicker: document.querySelector("select[id=gameSelector]"),
  role: document.querySelector("div[id=role]"),
  rolePicker: document.querySelector("select[id=roleSelector]"),
  name: document.querySelector("div[id=name]"),
  nameSelector: document.querySelector("input[id=nameSelector]"),
  relationship: document.querySelector("div[id=relationship]"),
  relationshipText: document.querySelector("p[id=relationshipText]"),
  relationshipDetails: document.querySelector("input[id=relationshipDetails]"),
  worldbuilding: document.querySelector("div[id=worldbuilding]"),
  worldbuildingText: document.querySelector("p[id=worldbuildingText]"),
  worldbuildingDetails: document.querySelector("input[id=worldbuildingDetails]"),
  quirk: document.querySelector("div[id=quirk]"),
  quirkPicker: document.querySelector("select[id=quirkSelector]"),
  quirkDetails: document.querySelector("input[id=quirkDetails]"),
  ability: document.querySelector("div[id=ability]"),
  abilityPicker: document.querySelector("select[id=abilitySelector]"),
  abilityDetails: document.querySelector("input[id=abilityDetails]"),
  objective: document.querySelector("div[id=objective]"),
  objectiveText: document.querySelector("p[id=objectiveText]"),
  objectiveDetails: document.querySelector("input[id=objectiveDetails]"),
  summary: document.querySelector("div[id=summary]"),
  uploadPlayerInput: document.querySelector("div[id=uploadPlayerInput]"),
  Roles: {}
};
hideEverything();
//$("#game").parent().show();

$.ajax({
    'async': true,
    'global': false,
    'url': "/games",
    'dataType': "json",
    'success': function (data) {
      console.log("read " + JSON.stringify(data));
      availableGames = data;

      $.ajax({
          'async': true,
          'global': false,
          'url': "templates/game.handlebars",
          'dataType': "text",
          'success': function (data) {
            console.log("read " + data);
            PraxisGlobal.game.innerHTML = data;
            console.log(`availableGames ${JSON.stringify(availableGames)}`);
            var source = $("#game").html();
            Handlebars.registerHelper('ifodd', function(v, options) {
              if(v % 2) {
                return options.inverse(this);
              }
              return options.fn(this);
            });
            Handlebars.registerHelper('times', function(n, block) {
              var accum = '';
              for(var i = 0; i < n; ++i)
                accum += block.fn(i);
              return accum;
            });
            Handlebars.registerHelper("repeat", function (times, opts) {
              var out = "";
              var i;
              var data = {};

              if( times ) {
                  for( i = 0; i < times; i += 1 ) {
                      data.index = i;
                      out += opts.fn(this, {
                          data: data
                      });
                  }
              } else {
                  out = opts.inverse(this);
              }

              return out;
            });
            var template = Handlebars.compile(source);
            var context = {
              "Games": availableGames
            };
            var html = template(context);
            PraxisGlobal.game.innerHTML = html;
            PraxisGlobal.gamePicker = document.querySelector("select[id=gameSelector]");
          }
      });
    }
});

function onGameSelected() {
  var x = PraxisGlobal.gamePicker.value;
  console.log(`Game picked: ${x}`);
  var gameRoles = null;
  PraxisGlobal.rolePicker = document.querySelector("select[id=roleSelector]");
  $.ajax({
      'async': true,
      'global': false,
      'url': `/games/${x}/roles`,
      'dataType': "json",
      'success': function (data) {
        console.log("read " + JSON.stringify(data));
        gameRoles = data;
        for( let r of data ) {
          const myOption = document.createElement("option");
          myOption.text = r;
          myOption.value = r;
          PraxisGlobal.rolePicker.appendChild(myOption);
        }
        // make the combo box selectable
        PraxisGlobal.rolePicker.enabled = true;
      }
  });

  //PraxisGlobal.game.hidden = true;
  $("#game").parent().hide();
  //PraxisGlobal.role.hidden = false;
  $("#role").parent().show();
}

function onRoleSelected() {
  var x = PraxisGlobal.rolePicker.value;
  console.log(`Role picked: ${x}`);
  PraxisGlobal.chosenRole = x;
  PraxisGlobal.nameSelector = document.querySelector("input[id=nameSelector]");

  //PraxisGlobal.role.hidden = true;
  $("#role").parent().hide();
  //PraxisGlobal.name.hidden = false;
  $("#name").parent().show();
  document.getElementById('nameSelector').focus();
}

function onNameSelected() {
  console.dir(PraxisGlobal.Roles);
  console.log(`chosen role ${PraxisGlobal.chosenRole} ${JSON.stringify(PraxisGlobal.Roles)}`);
  console.dir(PraxisGlobal.Roles[PraxisGlobal.chosenRole]);
  PraxisGlobal.objectiveIndex = Math.floor(Math.random() * PraxisGlobal.Roles[PraxisGlobal.chosenRole].objectives.length);
  let randomObjective = PraxisGlobal.Roles[PraxisGlobal.chosenRole].objectives[PraxisGlobal.objectiveIndex];
  console.log(`random objective ${randomObjective}`);
  PraxisGlobal.objectiveText.innerHTML = randomObjective;
  // make the input box selectable
  PraxisGlobal.objectiveDetails.enabled = true;
  document.getElementById('objectiveDetails').focus();
}

function onObjectiveSelected() {
  let chosenRole = PraxisGlobal.Roles[PraxisGlobal.rolePicker.value];
  PraxisGlobal.relationshipIndex = Math.floor(Math.random() * chosenRole.relationships.length);
  let randomRelationship = chosenRole.relationships[PraxisGlobal.relationshipIndex];
  PraxisGlobal.relationshipText.innerHTML = randomRelationship;
  //PraxisGlobal.objective.hidden = true;
  $("#objective").parent().hide();
  //PraxisGlobal.relationship.hidden = false;
  $("#relationship").parent().show();
  document.getElementById('relationshipDetails').focus();
}

function onRelationshipSelected() {
  let chosenRole = PraxisGlobal.Roles[PraxisGlobal.rolePicker.value];

  let randomQuestion = chosenRole.world_building[Math.floor(Math.random() * chosenRole.world_building.length)];
  console.log(`random question ${randomQuestion}`);
  PraxisGlobal.worldbuildingText.innerHTML = randomQuestion;

  //PraxisGlobal.relationship.hidden = true;
  $("#relationship").parent().hide();
  //PraxisGlobal.worldbuilding.hidden = false;
  $("#worldbuilding").parent().show();
  document.getElementById('worldbuildingDetails').focus();
}

function onWorldBuildingQuestionAnswered() {

  //PraxisGlobal.worldbuilding.hidden = true;
  $("#worldbuilding").parent().hide();
  //PraxisGlobal.quirkDetails.hidden = true;
  $("#quirkDetails").hide();
  //PraxisGlobal.quirk.hidden = false;
  $("#quirk").parent().parent().show();

  let chosenQuirk = null;
  for( let q of PraxisGlobal.Roles[PraxisGlobal.rolePicker.value].quirks ) {
    const myOption = document.createElement("option");
    myOption.text = q;
    myOption.value = q;
    PraxisGlobal.quirkPicker.appendChild(myOption);
  }

}

function onQuirkSelected() {
  console.log(`you selected ${PraxisGlobal.quirkPicker.value}`);
  //PraxisGlobal.quirkDetails.hidden = false;
  $("#quirkDetails").show();
  document.getElementById('quirkDetails').focus();
}

function onAbilitySelected() {
  console.log(`you selected ${PraxisGlobal.abilityPicker.value}`);
  //PraxisGlobal.abilityDetails.hidden = false;
  $("#abilityDetails").show();
  document.getElementById('abilityDetails').focus();
}

function onNameSelectorKeyDown(e) {
  if (e.keyCode === 13) {
    // load the next screen
    console.log(`/game/${PraxisGlobal.gamePicker.value}/roles/${PraxisGlobal.rolePicker.value}`);
    $.ajax({
        'async': true,
        'global': false,
        'url': `/games/${PraxisGlobal.gamePicker.value}/roles/${PraxisGlobal.rolePicker.value}`,
        'dataType': "json",
        'success': function (data) {
          console.log("read " + JSON.stringify(data));
          PraxisGlobal.Roles[PraxisGlobal.rolePicker.value] = data;
          //PraxisGlobal.name.hidden = true;
          $("#name").parent().hide();
          //PraxisGlobal.objective.hidden = false;
          $("#objective").parent().show();
          PraxisGlobal.objectiveDetails.enabled = false;
          onNameSelected();
        }
    });
  }
}

function onObjectDetailsKeyDown(e) {
  if (e.keyCode === 13) {
    // load the next screen
    onObjectiveSelected();
  }
}

function onRelationshipDetailsKeyDown(e) {
  if (e.keyCode === 13) {
    // load the next screen
    onRelationshipSelected();
  }
}

function onWorldBuildingDetailsKeyDown(e) {
  if (e.keyCode === 13) {
    // load the next screen
    onWorldBuildingQuestionAnswered();
  }
}

function onQuirkDetailsKeyDown(e) {
  if( e.keyCode === 13 ) {
    let chosenAbility = null;
    for( let q of PraxisGlobal.Roles[PraxisGlobal.rolePicker.value].abilities ) {
      const myOption = document.createElement("option");
      myOption.text = q;
      myOption.value = q;
      PraxisGlobal.abilityPicker.appendChild(myOption);
    }

    //PraxisGlobal.abilityDetails.hidden = true;
    $("#abilityDetails").hide();
    //PraxisGlobal.ability.hidden = false;
    $("#ability").parent().parent().show();
    //PraxisGlobal.quirk.hidden = true;
    $("#quirk").parent().parent().hide();
    //PraxisGlobal.quirkDetails.hidden = true;
    $("#quirkDetails").hide();
    // onQuirkSelected();
  }
}

function onAbilityDetailsKeyDown(e) {
  if( e.keyCode === 13 ) {
    console.log(`ability details entered`);
    //PraxisGlobal.ability.hidden = true;
    $("#ability").parent().parent().hide();
    //PraxisGlobal.abilityDetails.hidden = true;
    $("#abilityDetails").hide();
    console.log(` objectiveText ${PraxisGlobal.objectiveText.innerHTML}`);

    $.ajax({
        'async': true,
        'global': false,
        'url': "templates/summary.handlebars",
        'dataType': "text",
        'success': function (data) {
          console.log("read " + JSON.stringify(data));
          const suitIcons = [ "img/clubs.png", "img/diamonds.png", "img/hearts.png", "img/spades.png"];
          const context = {
            "Role": PraxisGlobal.rolePicker.value,
            "Name": PraxisGlobal.nameSelector.value,
            "RoleDescription": PraxisGlobal.Roles[PraxisGlobal.rolePicker.value].description,
            "Objective": PraxisGlobal.objectiveText.innerHTML,
            "ObjectiveIcon": suitIcons[PraxisGlobal.objectiveIndex],
            "ObjectiveDetails": PraxisGlobal.objectiveDetails.value,
            "Relationship": PraxisGlobal.relationshipText.innerHTML,
            "RelationshipIcon": suitIcons[PraxisGlobal.relationshipIndex],
            "RelationshipDetails": PraxisGlobal.relationshipDetails.value,
            "WorldBuildingQuestion": PraxisGlobal.worldbuildingText.innerHTML,
            "WorldBuildingAnswer": PraxisGlobal.worldbuildingDetails.value,
            "Quirk": PraxisGlobal.quirkPicker.value,
            "QuirkDetails": PraxisGlobal.quirkDetails.value,
            "Ability": PraxisGlobal.abilityPicker.value,
            "AbilityDetails": PraxisGlobal.abilityDetails.value,
            "Scenes": PraxisGlobal.Roles[PraxisGlobal.rolePicker.value].Scenes,
            "Milestones": PraxisGlobal.Roles[PraxisGlobal.rolePicker.value].Milestones,
            "Trepidations": PraxisGlobal.Roles[PraxisGlobal.rolePicker.value].Trepidations
          };
          loadPlayer(context);
        }
    });
  }
}

function onCheckboxClicked(e) {
  console.dir(e);
  console.log(`checkbox clicked`);
  if( e.currentTarget.state === "checked" ) {
    e.currentTarget.src = "img/unchecked.png";
    e.currentTarget.state = "unchecked";
  } else {
    e.currentTarget.src = "img/checked.png";
    e.currentTarget.state = "checked";
  }
}

function onDownloadClicked(e) {
  console.log(`downloading character ${JSON.stringify(PraxisGlobal.character)}`);
  // change Milestones, Trepidations, and scenes
  PraxisGlobal.character.Milestones = PraxisGlobal.character.Milestones.map((v, i, a) => {
    console.log(`i ${i}`)
    v.checked = $(`img#milestone-${i}.checkbox`)[0].state === "checked";
    return v;
  });
  PraxisGlobal.character.Trepidations = PraxisGlobal.character.Trepidations.map((v, i, a) => {
    v.checked = $(`img#trepidation-${i}.checkbox`)[0].state === "checked";
    return v;
  });
  PraxisGlobal.character.Scenes = PraxisGlobal.character.Scenes.map((v, i, a) => {
    v.checkedCount = 0;
    for( let s of $(`img#scene${i}.checkbox`) ) {
      if( s.state === "checked" ) {
        v.checkedCount = v.checkedCount + 1;
      }
    }
    return v;
  });
  download(JSON.stringify(PraxisGlobal.character, null, 2));
}

function download(text) {
  console.log(`downloading ${PraxisGlobal.character.Name}.json`)
  var element = document.createElement('a');
  element.setAttribute('href', 'data:application/octet-stream,' + encodeURIComponent(text));
  element.setAttribute('download', `${PraxisGlobal.character.Name}.json`);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

$('#newcharacter').click(onNewCharacterKeyDown);
$('#loadcharacter').click(onLoadCharacterKeyDown);
$('#nameSelector').keydown(onNameSelectorKeyDown);
$('#objectiveDetails').keydown(onObjectDetailsKeyDown);
$('#relationshipDetails').keydown(onRelationshipDetailsKeyDown);
$('#worldbuildingDetails').keydown(onWorldBuildingDetailsKeyDown);
$('#quirkDetails').keydown(onQuirkDetailsKeyDown);
$('#abilityDetails').keydown(onAbilityDetailsKeyDown);

document.getElementById('uploadPlayerInput_alt')
  .addEventListener('click',function() {
    document.getElementById('uploadPlayerInput').click();
  });

$("#uploadPlayerInput").change(onFileChosen);

function onNewCharacterKeyDown() {
  $("#landingpage").hide();
  $("#game").parent().show();
}

function onLoadCharacterKeyDown() {
  document.getElementById('uploadPlayerInput').click();
}

function onFileChosen() {
  console.log("upload file chosen, ${this.files[0]}");
  // will log a FileList object, view gifs below
  var reader = new FileReader();
  // inject an image with the src url
  reader.onload = function(event) {
    try {
      PraxisGlobal.uploadedFile = JSON.parse(event.target.result);
    } catch(err) {
      return alert(err.message);
    }
    // validate the uploaded file
    try {
      validateUploadedPlayer(PraxisGlobal.uploadedFile);
    } catch(err) {
      delete PraxisGlobal.uploadedFile;
      return alert(`error uploading player\n${err.message}`);
    }
    loadPlayer(PraxisGlobal.uploadedFile);
    console.log(`uploaded file ${JSON.stringify(PraxisGlobal.uploadedFile, null, 2)}`);
  }
  // when the file is read it triggers the onload event above.
  reader.readAsText(this.files[0]);
}

function checkForProperty(obj, prop) {
  if( !obj.hasOwnProperty(prop) ) {
    throw new Error(`missing property ${prop}`);
  }
}

function validateUploadedPlayer(player) {
  const requiredProperties = [ "Role", "Name", "RoleDescription", "Objective",
    "ObjectiveIcon", "ObjectiveDetails", "Relationship", "RelationshipIcon",
    "RelationshipDetails", "WorldBuildingQuestion", "WorldBuildingAnswer",
    "Quirk", "QuirkDetails", "Ability", "AbilityDetails", "Scenes", "Milestones",
    "Trepidations"];
  requiredProperties.map((v) => {
    console.log(`checking for ${v}`);
    checkForProperty(player, v);
  });
  $("#landingpage").hide();
}

function loadPlayer(player) {
  $.ajax({
      'async': true,
      'global': false,
      'url': "templates/summary.handlebars",
      'dataType': "text",
      'success': function (data) {
        console.log("read " + JSON.stringify(data));
        PraxisGlobal.summary.innerHTML = data;
        var source   = $("#summary").html();
        Handlebars.registerHelper('ifodd', handlebarsIfOddHelper);
        Handlebars.registerHelper('times', handlebarsTimesHelper);
        Handlebars.registerHelper("repeat", handlebarsRepeatHelper);

        var template = Handlebars.compile(source);

        PraxisGlobal.character = player;
        console.dir(player);
        var html = template(player);
        console.log(`html ${html}`);
        PraxisGlobal.summary.innerHTML = html;

        hideEverything();
        //PraxisGlobal.summary.hidden = false;
        $("#summary").show();
        $('.checkbox').on('click', onCheckboxClicked);
        $('#download').on('click', onDownloadClicked);
      }
  });
}

function handlebarsIfOddHelper(v, options) {
  if(v % 2) {
    return options.inverse(this);
  }
  return options.fn(this);
}

function handlebarsTimesHelper(n, block) {
  var accum = '';
  for(var i = 0; i < n; ++i)
    accum += block.fn(i);
  return accum;
}

function handlebarsRepeatHelper(times, opts) {
  var out = "";
  var i;
  var data = {};

  if( times ) {
      for( i = 0; i < times; i += 1 ) {
          data.index = i;
          out += opts.fn(this, {
              data: data
          });
      }
  } else {
      out = opts.inverse(this);
  }

  return out;
}

function hideEverything() {
  $("#game").parent().hide();
  $("#role").parent().hide();
  $("#objective").parent().hide();
  $("#relationship").parent().hide();
  $("#name").parent().hide();
  $("#worldbuilding").parent().hide();
  $("#quirk").parent().parent().hide();
  $("#quirkDetails").hide();
  $("#ability").parent().parent().hide();
  $("#abilityDetails").hide();
  $("#uploadPlayerInput_alt").hide();
}
